// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import (
	"fmt"
	"strings"
)

// Option is an interface describing an option that may be passed to
// any of the assert functions to modify how the the assertion is
// handled.
type Option interface {
	// Apply applies the option.
	Apply(ao *options)
}

// options contains the digested set of options.
type options struct {
	Msg   string        // Message to include with the assertion failure
	Args  []interface{} // Arguments for formatting message
	Fatal bool          // Flag indicating assertion failure is fatal
}

// parseOptions parses the options passed to AssertThat and constructs
// an assertOptions instance from it.
func parseOptions(opts []Option) options {
	result := options{}
	for _, opt := range opts {
		opt.Apply(&result)
	}
	return result
}

// format formats the message with its arguments.
func (ao options) format() string {
	if len(ao.Args) > 0 {
		return fmt.Sprintf(ao.Msg, ao.Args...)
	}

	return ao.Msg
}

// labeledItem is a structure that combines a label with content.
type labeledItem struct {
	Label   string // The content label
	Content string // The actual content
}

// format formats a single item.  The labelLen parameter specifies the
// length of the field to use for the label.  It returns a list of
// lines.
func (li labeledItem) format(labelLen int) []string {
	lines := strings.Split(li.Content, "\n")
	result := make([]string, len(lines))
	for i, line := range lines {
		if i == 0 {
			result[i] = fmt.Sprintf("%-*s: %s", labelLen, li.Label, line)
		} else {
			result[i] = fmt.Sprintf("%*s  %s", labelLen, "", line)
		}
	}
	return result
}

// content is a list of labeled items.
type content []labeledItem

// labelLen returns the longest label length from the content list.
func (c content) labelLen() int {
	longestLen := 0
	for _, item := range c {
		if len(item.Label) > longestLen {
			longestLen = len(item.Label)
		}
	}
	return longestLen
}

// format formats the list of content items into a single string.
func (c content) format() string {
	longestLen := c.labelLen()
	result := []string{}
	for _, li := range c {
		result = append(result, li.format(longestLen)...)
	}
	return strings.Join(result, "\n")
}

// Report reports an assertion failure.
func (ao options) Report(t T, err error) {
	// We're a helper function
	t.Helper()

	// Initialize the content
	c := content{
		{"Trace", strings.Join(callerInfo(), "\n")},
		{"Error", err.Error()},
		{"Test", t.Name()},
	}

	// Add the message
	if ao.Msg != "" {
		c = append(c, labeledItem{"Message", ao.format()})
	}

	// Log it
	t.Logf("\n%s", c.format())

	// Is it a fatal error?
	if ao.Fatal {
		t.FailNow()
	} else {
		t.Fail()
	}
}

// MsgOption is an Option implementation that sets an auxiliary
// message to include with an assertion failure.
type MsgOption struct {
	Msg  string        // Message to include with the assertion failure
	Args []interface{} // Arguments for formatting message
}

// Apply applies the option.
func (mo MsgOption) Apply(ao *options) {
	ao.Msg = mo.Msg
	ao.Args = mo.Args
}

// Msg constructs a MsgOption from a format string and optional
// arguments.
func Msg(format string, args ...interface{}) MsgOption {
	return MsgOption{
		Msg:  format,
		Args: args,
	}
}

// FatalOption is an Option implementation that sets the Fatal field.
type FatalOption bool

// Apply applies the option.
func (fo FatalOption) Apply(ao *options) {
	ao.Fatal = bool(fo)
}

// Fatal is an Option that indicates that a failure of the assertion
// should be considered fatal.  This should be used if a later
// assertion would crash the test, e.g., if an assertion is verifying
// that a value is not nil, and a later assertion would derefence the
// pointer.
var Fatal = FatalOption(true)
