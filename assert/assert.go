// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import "gitlab.com/klmitch/trial/expect"

// That checks an expectation.
func That(t T, e expect.Expectation, actual interface{}, opts ...Option) {
	// We're a helper function
	t.Helper()

	// Check the expectation
	err := e.Check(actual)
	if err == nil {
		return
	}

	// Assertion failed; parse the options and report the error
	ao := parseOptions(opts)
	ao.Report(t, err)
}

// Patch points for testing the units in this package.
var (
	that = That
)
