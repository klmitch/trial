// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import (
	"bytes"
	"fmt"
)

func TryRecover(f func()) (result interface{}) {
	defer func() {
		if panicData := recover(); panicData != nil {
			result = panicData
		}
	}()
	f()
	return nil
}

type mockT struct {
	TestName     string
	HelperCalled bool
	LogBuf       *bytes.Buffer
	Failed       bool
}

func (m *mockT) Fail() {
	m.Failed = true
}

func (m *mockT) FailNow() {
	m.Failed = true
	panic("failing test now")
}

func (m *mockT) Helper() {
	m.HelperCalled = true
}

func (m *mockT) Logf(format string, args ...interface{}) {
	if m.LogBuf == nil {
		m.LogBuf = &bytes.Buffer{}
	}

	fmt.Fprintf(m.LogBuf, format, args...)
}

func (m *mockT) Name() string {
	return m.TestName
}
