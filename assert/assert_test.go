// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import (
	"testing"

	"gitlab.com/klmitch/trial/expect"
)

func TestThatBase(t *testing.T) {
	saveCallerInfo := callerInfo
	callerInfo = func() []string {
		return []string{"file1:1", "file2:2", "file3:3"}
	}
	t.Cleanup(func() {
		callerInfo = saveCallerInfo
	})
	theT := &mockT{
		TestName: "TestFoo",
	}

	That(theT, expect.Anything, 42)

	if !theT.HelperCalled {
		t.Error("AssertThat failed to set Helper")
	}
	if theT.Failed {
		t.Error("AssertThat unexpectedly reported failure")
	}
}

func TestThatReport(t *testing.T) {
	saveCallerInfo := callerInfo
	callerInfo = func() []string {
		return []string{"file1:1", "file2:2", "file3:3"}
	}
	t.Cleanup(func() {
		callerInfo = saveCallerInfo
	})
	theT := &mockT{
		TestName: "TestFoo",
	}

	That(theT, expect.Func(func(_ interface{}) error { return AnError }), 42)

	if !theT.HelperCalled {
		t.Error("AssertThat failed to set Helper")
	}
	if !theT.Failed {
		t.Error("AssertThat failed to report failure")
	}
}
