// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import "gitlab.com/klmitch/trial/internal/common"

// Patch points used to allow testing of trial units in isolation.
var (
	callerInfo = common.CallerInfo
)

// T is an interface matching the elements of testing.T that we
// require.
type T interface {
	// Helper marks the calling function as a test helper
	// function.
	Helper()

	// Name returns the name of the running test.
	Name() string

	// Logf formats its arguments according to the format and
	// records the text in the error log.
	Logf(format string, args ...interface{})

	// Fail marks the function as having failed but continues
	// execution.
	Fail()

	// FailNow morks the function as having failed and stops its
	// execution.
	FailNow()
}

// AnError is an error that may be used when an arbitrary error is
// required.
var AnError = common.AnError
