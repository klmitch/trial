// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import (
	"reflect"
	"testing"
)

type mockOption struct {
	ao *options
}

func (m *mockOption) Apply(ao *options) {
	m.ao = ao
}

func TestParseOptions(t *testing.T) {
	opt1 := &mockOption{}
	opt2 := &mockOption{}

	result := parseOptions([]Option{opt1, opt2})

	if result.Msg != "" || len(result.Args) > 0 || result.Fatal {
		t.Errorf("parseOptions returned %+v", result)
	}
	if opt1.ao == nil {
		t.Error("opt1 not called")
	}
	if opt2.ao == nil {
		t.Error("opt2 not called")
	}
}

func TestOptionsFormatMessageOnly(t *testing.T) {
	obj := options{
		Msg: "message",
	}

	result := obj.format()

	if result != "message" { //nolint:goconst
		t.Errorf("expected \"message\", got %q", result)
	}
}

func TestOptionsFormatMessageAndArguments(t *testing.T) {
	obj := options{
		Msg:  "mess%s %d",
		Args: []interface{}{"age", 5},
	}

	result := obj.format()

	if result != "message 5" {
		t.Errorf("expected \"message 5\", got %q", result)
	}
}

func TestLabeledItemFormatBase(t *testing.T) {
	obj := labeledItem{"label", "some text"}

	result := obj.format(7)

	if len(result) != 1 {
		t.Errorf("format returned %d lines, expected 1", len(result))
	}
	if result[0] != "label  : some text" {
		t.Errorf("line 0: expected \"label  : some text\", got %q", result[0])
	}
}

func TestLabeledItemFormatMultipleLine(t *testing.T) {
	obj := labeledItem{"label", "some text\nline 2\nline 3"}

	result := obj.format(7)

	if len(result) != 3 {
		t.Errorf("format returned %d lines, expected 3", len(result))
	}
	if result[0] != "label  : some text" {
		t.Errorf("line 0: expected \"label  : some text\", got %q", result[0])
	}
	if result[1] != "         line 2" {
		t.Errorf("line 1: expected \"         line 2\", got %q", result[1])
	}
	if result[2] != "         line 3" {
		t.Errorf("line 2: expected \"         line 3\", got %q", result[2])
	}
}

func TestContentLabelLen(t *testing.T) {
	obj := content{
		{Label: "1"},
		{Label: "12345"},
		{Label: "123"},
	}

	result := obj.labelLen()

	if result != 5 {
		t.Errorf("labelLen returned %d, expected 5", result)
	}
}

func TestContentFormat(t *testing.T) {
	obj := content{
		{"foo", "this is the foo content"},
		{"bar", "this is\nmulti-line\nbar"},
		{"quux", "this is quux"},
	}

	result := obj.format()

	if result != "foo : this is the foo content\nbar : this is\n      multi-line\n      bar\nquux: this is quux" {
		t.Errorf("format returned %q", result)
	}
}

func TestOptionsReportBase(t *testing.T) {
	obj := options{}
	saveCallerInfo := callerInfo
	callerInfo = func() []string {
		return []string{"file1:1", "file2:2", "file3:3"}
	}
	t.Cleanup(func() {
		callerInfo = saveCallerInfo
	})
	theT := &mockT{
		TestName: "TestFoo",
	}

	obj.Report(theT, AnError)

	if !theT.HelperCalled {
		t.Error("Report failed to set Helper")
	}
	if !theT.Failed {
		t.Error("Report failed to set Failed")
	}
	if theT.LogBuf == nil {
		t.Error("Report failed to log output")
	} else if theT.LogBuf.String() != "\nTrace: file1:1\n       file2:2\n       file3:3\nError: an error\nTest : TestFoo" {
		t.Errorf("Report output incorrect; actual: %q", theT.LogBuf.String())
	}
}

func TestOptionsReportWithMessage(t *testing.T) {
	obj := options{
		Msg: "message",
	}
	saveCallerInfo := callerInfo
	callerInfo = func() []string {
		return []string{"file1:1", "file2:2", "file3:3"}
	}
	t.Cleanup(func() {
		callerInfo = saveCallerInfo
	})
	theT := &mockT{
		TestName: "TestFoo",
	}

	obj.Report(theT, AnError)

	if !theT.HelperCalled {
		t.Error("Report failed to set Helper")
	}
	if !theT.Failed {
		t.Error("Report failed to set Failed")
	}
	if theT.LogBuf == nil {
		t.Error("Report failed to log output")
	} else if theT.LogBuf.String() != "\nTrace  : file1:1\n         file2:2\n         file3:3\nError  : an error\nTest   : TestFoo\nMessage: message" {
		t.Errorf("Report output incorrect; actual: %q", theT.LogBuf.String())
	}
}

func TestOptionsReportFatal(t *testing.T) {
	obj := options{
		Fatal: true,
	}
	saveCallerInfo := callerInfo
	callerInfo = func() []string {
		return []string{"file1:1", "file2:2", "file3:3"}
	}
	t.Cleanup(func() {
		callerInfo = saveCallerInfo
	})
	theT := &mockT{
		TestName: "TestFoo",
	}

	panicData := TryRecover(func() { obj.Report(theT, AnError) })

	if panicData == nil {
		t.Error("Report failed to call FailNow")
	} else if panicData != "failing test now" {
		t.Errorf("Report paniced for an unexpected reason: %+v", panicData)
	}
	if !theT.HelperCalled {
		t.Error("Report failed to set Helper")
	}
	if !theT.Failed {
		t.Error("Report failed to set Failed")
	}
	if theT.LogBuf == nil {
		t.Error("Report failed to log output")
	} else if theT.LogBuf.String() != "\nTrace: file1:1\n       file2:2\n       file3:3\nError: an error\nTest : TestFoo" {
		t.Errorf("Report output incorrect; actual: %q", theT.LogBuf.String())
	}
}

func TestMsgOptionImplementsOption(t *testing.T) {
	iface := reflect.TypeOf((*Option)(nil)).Elem()

	if !reflect.TypeOf(MsgOption{}).Implements(iface) {
		t.Error("MsgOption must implement Option")
	}
}

func TestMsgOptionApply(t *testing.T) {
	obj := MsgOption{
		Msg:  "message",
		Args: []interface{}{"one", 2, "three"},
	}
	ao := options{}

	obj.Apply(&ao)

	if ao.Msg != "message" {
		t.Errorf("ao.Msg set to %q, not \"message\"", ao.Msg)
	}
	if len(ao.Args) != len(obj.Args) {
		t.Errorf("ao.Args has length %d, should be %d", len(ao.Args), len(obj.Args))
	} else {
		for i, arg := range ao.Args {
			if arg != obj.Args[i] {
				t.Errorf("Argument %d: should be %+v, is actually %+v", i, obj.Args[i], arg)
			}
		}
	}
}

func TestMsg(t *testing.T) {
	result := Msg("message", "one", 2, "three")

	if result.Msg != "message" {
		t.Errorf("Msg set to %q, not \"message\"", result.Msg)
	}
	if len(result.Args) != 3 {
		t.Errorf("Args has length %d, should be 3", len(result.Args))
	} else {
		for i, arg := range []interface{}{"one", 2, "three"} {
			if result.Args[i] != arg {
				t.Errorf("Argument %d: should be %+v, is actually %+v", i, arg, result.Args[i])
			}
		}
	}
}

func TestFatalOptionImplementsOption(t *testing.T) {
	iface := reflect.TypeOf((*Option)(nil)).Elem()

	if !reflect.TypeOf(Fatal).Implements(iface) {
		t.Error("FatalOption must implement Option")
	}
}

func TestFatalOptionApply(t *testing.T) {
	ao := options{}

	Fatal.Apply(&ao)

	if !ao.Fatal {
		t.Error("Apply failed to set Fatal")
	}
}
