// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import "gitlab.com/klmitch/trial/expect"

// Equal compares the actual value to the expected value, verifying
// the assertion that they are equal.
func Equal(t T, expected, actual interface{}, opts ...Option) {
	// We're a helper function
	t.Helper()

	// Construct the expectation
	e := expect.Equal(expected)

	// Check it
	that(t, e, actual, opts...)
}

// NotEqual compares the actual value to the expected value, verifying
// the assertion that they are not equal.
func NotEqual(t T, expected, actual interface{}, opts ...Option) {
	// We're a helper function
	t.Helper()

	// Construct the expectation
	e := expect.NotEqual(expected)

	// Check it
	that(t, e, actual, opts...)
}
