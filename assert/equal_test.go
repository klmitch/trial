// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package assert

import (
	"testing"

	"gitlab.com/klmitch/trial/expect"
)

func TestEqual(t *testing.T) {
	theT := &mockT{}
	saveThat := that
	var e expect.Expectation
	var actual interface{}
	that = func(tt T, ee expect.Expectation, aa interface{}, _ ...Option) {
		if tt != theT {
			t.Error("that not called with T value")
		}
		e = ee
		actual = aa
	}
	t.Cleanup(func() {
		that = saveThat
	})

	Equal(theT, 42, 42)

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if e.(expect.EqualExpectation).Expected != 42 {
		t.Errorf("failed to construct correct expectation; was %#v", e)
	}
	if actual != 42 {
		t.Errorf("failed to pass correct actual; was %#v", actual)
	}
}

func TestNotEqual(t *testing.T) {
	theT := &mockT{}
	saveThat := that
	var e expect.Expectation
	var actual interface{}
	that = func(tt T, ee expect.Expectation, aa interface{}, _ ...Option) {
		if tt != theT {
			t.Error("that not called with T value")
		}
		e = ee
		actual = aa
	}
	t.Cleanup(func() {
		that = saveThat
	})

	NotEqual(theT, 42, 42)

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	ne, ok := e.(expect.NotExpectation)
	if !ok {
		t.Errorf("failed to construct correct expectation; was %#v", e)
	} else if ne.Wrapped.(expect.EqualExpectation).Expected != 42 {
		t.Errorf("failed to construct correct expectation; was %#v", e)
	}
	if actual != 42 {
		t.Errorf("failed to pass correct actual; was %#v", actual)
	}
}
