// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"bytes"
	"fmt"
	"reflect"

	"gitlab.com/klmitch/trial/util"
)

// EqualError is an error returned by the Equal expectation.  It
// encapsulates the expected and actual values, and dynamically
// constructs the differences to emit when the Error method is called.
type EqualError struct {
	ExpectationError

	Expected interface{} // Expected value
}

// Error returns the error message.
func (e EqualError) Error() string {
	return fmt.Sprintf("%s; expected %#v, actual value: %#v", ErrExpectationFailed, e.Expected, e.Actual)
}

// EqualExpectation is an implementation of Expectation that signifies
// that the actual value is expected to equal a specified expected
// value.
type EqualExpectation struct {
	Expected interface{}   // The expected value
	From     util.Location // Where the expectation was created
}

// mkErr is a helper function that constructs and returns an
// EqualError, given an actual value.
func (ee EqualExpectation) mkErr(actual interface{}) error {
	return WithLoc(ee, EqualError{
		ExpectationError: ExpectationError{
			Actual: actual,
		},
		Expected: ee.Expected,
	})
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (ee EqualExpectation) Check(actual interface{}) error {
	// Handle nils first
	if ee.Expected == nil && actual == nil {
		return nil
	}

	// If either argument is a function, bail out
	if isFunction(ee.Expected) || isFunction(actual) {
		return ee.mkErr(actual)
	}

	// Check if we're expecting a byte array
	exp, ok := ee.Expected.([]byte)
	if !ok {
		// OK, not byte arrays; use reflect.DeepEqual
		if reflect.DeepEqual(ee.Expected, actual) {
			return nil
		}
		return ee.mkErr(actual)
	}

	// Were we passed a byte array?
	act, ok := actual.([]byte)
	if !ok {
		return ee.mkErr(actual)
	}

	// Could be nil byte arrays
	if exp == nil || act == nil {
		if exp == nil && act == nil {
			return nil
		}
		return ee.mkErr(actual)
	}

	// Compare byte arrays
	if bytes.Equal(exp, act) {
		return nil
	}
	return ee.mkErr(actual)
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (ee EqualExpectation) Location() util.Location {
	return ee.From
}

// Equal returns an expectation that specifies that actual values must
// be equal to the expected value passed to Equal.
func Equal(expected interface{}) EqualExpectation {
	return EqualExpectation{
		Expected: expected,
		From:     util.CalledFrom(0),
	}
}

// NotEqualError is an error returned by the NotEqual expectation.
// Like EqualError, it encapsulates the expected and actual values.
type NotEqualError struct {
	ExpectationError

	Expected interface{} // Expected value
}

// Error returns the error message.
func (e NotEqualError) Error() string {
	return fmt.Sprintf("%s; expected not equal to %#v, actual value: %#v", ErrExpectationFailed, e.Expected, e.Actual)
}

// NotEqual returns an expectation that specifies that actual values
// must not be equal to the expected value passed to NotEqual.
func NotEqual(expected interface{}) NotExpectation {
	return Not(Equal(expected), NotFactory(func(actual interface{}) error {
		return NotEqualError{
			ExpectationError: ExpectationError{
				Actual: actual,
			},
			Expected: expected,
		}
	}))
}
