// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"errors"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/klmitch/trial/util"
)

var (
	ErrTest1 = errors.New("test 1")
	ErrTest2 = errors.New("test 2")
	ErrTest3 = errors.New("test 3")
)

var notOptionIface = reflect.TypeOf((*NotOption)(nil)).Elem()

func TestNotErrorOptionImplementsNotOption(t *testing.T) {
	if !reflect.TypeOf(NotErrorOption{}).Implements(notOptionIface) {
		t.Error("NotErrorOption must implement NotOption")
	}
}

func TestNotErrorOptionApply(t *testing.T) {
	obj := NotErrorOption{
		Error: AnError,
	}
	ne := &NotExpectation{}

	obj.Apply(ne)

	if !errors.Is(ne.Error, AnError) {
		t.Errorf("Apply set Error to %#v", ne.Error)
	}
}

func TestNotError(t *testing.T) {
	result := NotError(AnError)

	if !errors.Is(result.Error, AnError) {
		t.Errorf("NotError set Error to %#v", result.Error)
	}
}

func TestNotFactoryOptionImplementsNotOption(t *testing.T) {
	if !reflect.TypeOf(NotFactoryOption{}).Implements(notOptionIface) {
		t.Error("NotFactoryOption must implement NotOption")
	}
}

func TestNotFactoryOptionApply(t *testing.T) {
	var called interface{}
	obj := NotFactoryOption{
		Factory: func(actual interface{}) error {
			called = actual
			return AnError
		},
	}
	ne := &NotExpectation{}

	obj.Apply(ne)

	if ne.Factory == nil {
		t.Error("Apply failed to set Factory")
	} else {
		err := ne.Factory(42)
		if !errors.Is(err, AnError) {
			t.Errorf("Factory returned unexpected error %#v", err)
		}
		if called != 42 {
			t.Error("incorrect Factory installed")
		}
	}
}

func TestNotFactory(t *testing.T) {
	var called interface{}

	result := NotFactory(func(actual interface{}) error {
		called = actual
		return AnError
	})

	if result.Factory == nil {
		t.Error("NotFactory failed to set Factory")
	} else {
		err := result.Factory(42)
		if !errors.Is(err, AnError) {
			t.Errorf("Factory returned unexpected error %#v", err)
		}
		if called != 42 {
			t.Error("incorrect factory installed")
		}
	}
}

func TestNotExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(NotExpectation{}).Implements(expectIface) {
		t.Error("NotExpectation must implement Expectation")
	}
	if !reflect.TypeOf(NotExpectation{}).Implements(locatableIface) {
		t.Error("NotExpectation must implement Locatable")
	}
}

func TestNotExpectationCheckBase(t *testing.T) {
	obj := NotExpectation{
		Wrapped: &FuncExpectation{
			Func: func(actual interface{}) error {
				if actual != 42 {
					t.Error("Func not called with actual value")
				}
				return AnError
			},
		},
		Error: ErrExpectationFailed,
	}

	err := obj.Check(42)
	if err != nil {
		t.Errorf("Check returned unexpected error %+v", err)
	}
}

func TestNotExpectationCheckFailure(t *testing.T) {
	obj := NotExpectation{
		Wrapped: Anything,
		Error:   ErrExpectationFailed,
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrExpectationFailed) {
		t.Errorf("Check did not return expected error: %#v", err)
	}
	if _, ok := err.(ExpectationError); ok {
		t.Errorf("Check returned ExpectationError(%#v)", err)
	}
}

func TestNotExpectationCheckFailureExpectationError(t *testing.T) {
	var called interface{}
	obj := NotExpectation{
		Wrapped: Anything,
		Factory: func(actual interface{}) error {
			called = actual
			return AnError
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, AnError) {
		t.Errorf("Check did not return expected error: %#v", err)
	}
	if called != 42 {
		t.Error("Check failed to call factory")
	}
}

func TestNotExpectationLocation(t *testing.T) {
	loc := util.Location{
		Line: 23,
	}
	obj := NotExpectation{
		From: loc,
	}

	result := obj.Location()

	if result != loc {
		t.Errorf("Location returned %#v, expected %#v", result, loc)
	}
}

func TestNotBase(t *testing.T) {
	result := Not(Anything)

	if result.Wrapped != Anything {
		t.Errorf("Not saved %+v instead of expected Anything", result.Wrapped)
	}
	if !errors.Is(result.Error, ErrExpectationFailed) {
		t.Errorf("Not saved error %+v instead of %+v", result.Error, ErrExpectationFailed)
	}
	if !strings.HasSuffix(result.From.File, "/expect/combinatorial_test.go") {
		t.Errorf("Not returned unexpected location %#v", result.From)
	}
}

func TestNotLocationFrom(t *testing.T) {
	e := mockLocatable{
		Loc: util.Location{
			Line: 42,
		},
	}
	result := Not(e)

	if result.Wrapped != e {
		t.Errorf("Not saved %+v instead of expected Anything", result.Wrapped)
	}
	if !errors.Is(result.Error, ErrExpectationFailed) {
		t.Errorf("Not saved error %+v instead of %+v", result.Error, ErrExpectationFailed)
	}
	if result.From.Line != 42 {
		t.Errorf("Not returned unexpected location %#v", result.From)
	}
}

func TestNotAlternateError(t *testing.T) {
	result := Not(Anything, NotError(AnError))

	if result.Wrapped != Anything {
		t.Errorf("Not saved %+v instead of expected Anything", result.Wrapped)
	}
	if !errors.Is(result.Error, AnError) {
		t.Errorf("Not saved error %+v instead of %+v", result.Error, AnError)
	}
}

func TestAndExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(AndExpectation{}).Implements(expectIface) {
		t.Error("AndExpectation must implement Expectation")
	}
	if !reflect.TypeOf(AndExpectation{}).Implements(locatableIface) {
		t.Error("AndExpectation must implement Locatable")
	}
}

func TestAndExpectationCheckBase(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := AndExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return nil
				},
			},
		},
	}

	err := obj.Check(42)
	if err != nil {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if !expect3Called {
		t.Error("expectation 3 not checked")
	}
}

func TestAndExpectationCheckFailure(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := AndExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return ErrTest2
				},
				From: util.Location{
					Line: 42,
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return nil
				},
			},
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrTest2) {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if expect3Called {
		t.Error("expectation 3 checked")
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestAndExpectationCheckFailureWithLocation(t *testing.T) {
	expect1Called := false
	expect3Called := false
	obj := AndExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return nil
				},
			},
			Nothing,
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return nil
				},
			},
		},
		From: util.Location{
			Line: 42,
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrExpectationFailed) {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if expect3Called {
		t.Error("expectation 3 checked")
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestAndExpectationLocation(t *testing.T) {
	loc := util.Location{
		Line: 23,
	}
	obj := AndExpectation{
		From: loc,
	}

	result := obj.Location()

	if result != loc {
		t.Errorf("Location returned %#v, expected %#v", result, loc)
	}
}

func TestAnd(t *testing.T) {
	result := And(mockExpectation{0}, mockExpectation{1}, mockExpectation{2})

	if len(result.Wrapped) != 3 {
		t.Errorf("And stored %d expectations instead of 3", len(result.Wrapped))
	} else {
		for i := 0; i < 3; i++ {
			if result.Wrapped[i] != (mockExpectation{i}) {
				t.Errorf("element %d: expected %+v, but %+v stored", i, mockExpectation{i}, result.Wrapped[i])
			}
		}
	}
	if !strings.HasSuffix(result.From.File, "/expect/combinatorial_test.go") {
		t.Errorf("And returned unexpected location %#v", result.From)
	}
}

func TestOrExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(OrExpectation{}).Implements(expectIface) {
		t.Error("OrExpectation must implement Expectation")
	}
	if !reflect.TypeOf(OrExpectation{}).Implements(locatableIface) {
		t.Error("OrExpectation must implement Locatable")
	}
}

func TestOrExpectationCheckBase(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := OrExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return ErrTest1
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return ErrTest3
				},
			},
		},
	}

	err := obj.Check(42)
	if err != nil {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if expect3Called {
		t.Error("expectation 3 checked")
	}
}

func TestOrExpectationCheckFailure(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := OrExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return ErrTest1
				},
				From: util.Location{
					Line: 42,
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return ErrTest2
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return ErrTest3
				},
			},
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrTest1) {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if !expect3Called {
		t.Error("expectation 3 not checked")
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestOrExpectationCheckFailureWithLocation(t *testing.T) {
	obj := OrExpectation{
		Wrapped: []Expectation{Nothing, Nothing, Nothing},
		From: util.Location{
			Line: 42,
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrExpectationFailed) {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestOrExpectationLocation(t *testing.T) {
	loc := util.Location{
		Line: 23,
	}
	obj := OrExpectation{
		From: loc,
	}

	result := obj.Location()

	if result != loc {
		t.Errorf("Location returned %#v, expected %#v", result, loc)
	}
}

func TestOr(t *testing.T) {
	result := Or(mockExpectation{0}, mockExpectation{1}, mockExpectation{2})

	if len(result.Wrapped) != 3 {
		t.Errorf("Or stored %d expectations instead of 3", len(result.Wrapped))
	} else {
		for i := 0; i < 3; i++ {
			if result.Wrapped[i] != (mockExpectation{i}) {
				t.Errorf("element %d: expected %+v, but %+v stored", i, mockExpectation{i}, result.Wrapped[i])
			}
		}
	}
	if !strings.HasSuffix(result.From.File, "/expect/combinatorial_test.go") {
		t.Errorf("And returned unexpected location %#v", result.From)
	}
}

func TestXorErrorImplementsErrorAndUnwrapper(t *testing.T) {
	if !reflect.TypeOf(XorError{}).Implements(errIface) {
		t.Error("XorError must implement error")
	}
	if !reflect.TypeOf(XorError{}).Implements(unwrapperIface) {
		t.Error("XorError must implement Unwrapper")
	}
}

func TestXorErrorError(t *testing.T) {
	obj := &XorError{
		ExpectationError: ExpectationError{
			Actual: 42,
		},
		Met: []Expectation{Anything, Anything, Anything},
	}

	result := obj.Error()

	expected := "expectation failed; actual value: 42; 3 expectations met"
	if result != expected {
		t.Errorf("Error returned %q instead of %q", result, expected)
	}
}

func TestXorExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(XorExpectation{}).Implements(expectIface) {
		t.Error("XorExpectation must implement Expectation")
	}
	if !reflect.TypeOf(XorExpectation{}).Implements(locatableIface) {
		t.Error("XorExpectation must implement Locatable")
	}
}

func TestXorExpectationCheckBase(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := XorExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return ErrTest1
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return ErrTest3
				},
			},
		},
	}

	err := obj.Check(42)
	if err != nil {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if !expect3Called {
		t.Error("expectation 3 not checked")
	}
}

func TestXorExpectationCheckNoPass(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := XorExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return ErrTest1
				},
				From: util.Location{
					Line: 42,
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return ErrTest2
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return ErrTest3
				},
			},
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, ErrTest1) {
		t.Errorf("Check returned unexpected error %+v", err)
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if !expect3Called {
		t.Error("expectation 3 not checked")
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestXorExpectationCheckMultiPass(t *testing.T) {
	expect1Called := false
	expect2Called := false
	expect3Called := false
	obj := XorExpectation{
		Wrapped: []Expectation{
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 1 not called with actual value")
					}
					expect1Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 2 not called with actual value")
					}
					expect2Called = true
					return nil
				},
			},
			&FuncExpectation{
				Func: func(actual interface{}) error {
					if actual != 42 {
						t.Error("function 3 not called with actual value")
					}
					expect3Called = true
					return ErrTest3
				},
			},
		},
		From: util.Location{
			Line: 42,
		},
	}

	err := obj.Check(42)

	var xe XorError
	if !errors.As(err, &xe) {
		t.Errorf("Check returned unexpected error %+v", err)
	} else {
		if xe.Actual != 42 {
			t.Errorf("Error contains actual %#v, should be 42", xe.Actual)
		}
		if len(xe.Met) != 2 || xe.Met[0] != obj.Wrapped[0] || xe.Met[1] != obj.Wrapped[1] {
			t.Errorf("Met expectations %#v, should be %#v", xe.Met, obj.Wrapped[0:2])
		}
	}
	if !expect1Called {
		t.Error("expectation 1 not checked")
	}
	if !expect2Called {
		t.Error("expectation 2 not checked")
	}
	if !expect3Called {
		t.Error("expectation 3 not checked")
	}
	loc, ok := Location(err)
	if !ok {
		t.Error("Check returned unlocatable error")
	} else if loc.Line != 42 {
		t.Errorf("Check unexpectedly returned error with location %#v", loc)
	}
}

func TestXor(t *testing.T) {
	result := Xor(mockExpectation{0}, mockExpectation{1}, mockExpectation{2})

	if len(result.Wrapped) != 3 {
		t.Errorf("Xor stored %d expectations instead of 3", len(result.Wrapped))
	} else {
		for i := 0; i < 3; i++ {
			if result.Wrapped[i] != (mockExpectation{i}) {
				t.Errorf("element %d: expected %+v, but %+v stored", i, mockExpectation{i}, result.Wrapped[i])
			}
		}
	}
	if !strings.HasSuffix(result.From.File, "/expect/combinatorial_test.go") {
		t.Errorf("And returned unexpected location %#v", result.From)
	}
}
