// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"errors"
	"reflect"
	"strings"
	"testing"

	"gitlab.com/klmitch/trial/internal/common"
	"gitlab.com/klmitch/trial/util"
)

var AnError = common.AnError

var (
	expectIface    = reflect.TypeOf((*Expectation)(nil)).Elem()
	locatableIface = reflect.TypeOf((*Locatable)(nil)).Elem()
)

func TestFuncExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(&FuncExpectation{}).Implements(expectIface) {
		t.Error("FuncExpectation must implement Expectation")
	}
	if !reflect.TypeOf(&FuncExpectation{}).Implements(locatableIface) {
		t.Error("FuncExpectation must implement Locatable")
	}
}

func TestFuncExpectationCheck(t *testing.T) {
	called := false
	obj := &FuncExpectation{
		Func: func(actual interface{}) error {
			if actual.(int) != 42 {
				t.Errorf("Function called with %v instead of 42", actual)
			}
			called = true
			return AnError
		},
	}

	err := obj.Check(42)

	if !errors.Is(err, AnError) {
		t.Error("Function did not return expected error")
	}
	if !called {
		t.Error("Function did not get called")
	}
}

func TestFuncExpectationLocation(t *testing.T) {
	loc := util.Location{
		Line: 23,
	}
	obj := &FuncExpectation{
		From: loc,
	}

	result := obj.Location()

	if result != loc {
		t.Errorf("Location returned %#v, expected %#v", result, loc)
	}
}

func TestFuncExpectationEqualBase(t *testing.T) {
	obj := &FuncExpectation{}
	other := obj

	result := obj.Equal(other)

	if !result {
		t.Error("Equal returned false")
	}
}

func TestFuncExpectationEqualNotEqual(t *testing.T) {
	obj := &FuncExpectation{}
	other := &FuncExpectation{}

	result := obj.Equal(other)

	if result {
		t.Error("Equal returned true")
	}
}

func TestFuncExpectationEqualWrongType(t *testing.T) {
	obj := &FuncExpectation{}
	other := Anything

	result := obj.Equal(other)

	if result {
		t.Error("Equal returned true")
	}
}

func TestFunc(t *testing.T) {
	called := false
	theFunc := func(actual interface{}) error {
		if actual != 42 {
			t.Error("Func not called with actual value")
		}
		called = true
		return AnError
	}

	result := Func(theFunc)

	if result.Func == nil {
		t.Error("Func did not populate Func")
	} else {
		_ = result.Func(42)
		if !called {
			t.Error("Func did not save correct function")
		}
	}
	if !strings.HasSuffix(result.From.File, "/expect/expect_test.go") {
		t.Errorf("Func returned unexpected location %#v", result.From)
	}
}

func TestAnythingExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(Anything).Implements(expectIface) {
		t.Error("AnythingExpectation must implement Expectation")
	}
}

func TestAnythingExpectationCheck(t *testing.T) {
	err := Anything.Check(42)
	if err != nil {
		t.Errorf("Function returned unexpected error %s", err)
	}
}

type mockExpectation struct {
	Value int
}

func (m mockExpectation) Check(_ interface{}) error {
	return nil
}

type mockLocatable struct {
	Value int
	Loc   util.Location
}

func (m mockLocatable) Check(_ interface{}) error {
	return nil
}

func (m mockLocatable) Location() util.Location {
	return m.Loc
}

type mockEqualer struct {
	Value    int
	EqualRet bool
	Other    Expectation
}

func (m mockEqualer) Check(_ interface{}) error {
	return nil
}

func (m *mockEqualer) Equal(other Expectation) bool {
	m.Other = other
	return m.EqualRet
}

func TestIsEqualBase(t *testing.T) {
	e1 := mockExpectation{Value: 5}
	e2 := mockExpectation{Value: 5}

	result := IsEqual(e1, e2)

	if !result {
		t.Error("IsEqual returned false")
	}
}

func TestIsEqualDifferent(t *testing.T) {
	e1 := mockExpectation{Value: 5}
	e2 := mockExpectation{Value: 6}

	result := IsEqual(e1, e2)

	if result {
		t.Error("IsEqual returned true")
	}
}

func TestIsEqualEqualerTrue(t *testing.T) {
	e1 := &mockEqualer{
		Value:    5,
		EqualRet: true,
	}
	e2 := &mockEqualer{Value: 6}

	result := IsEqual(e1, e2)

	if !result {
		t.Error("IsEqual returned false")
	}
	if e1.Other != e2 {
		t.Errorf("IsEqual called with %#v instead of %#v", e1.Other, e2)
	}
}

func TestIsEqualEqualerFalse(t *testing.T) {
	e1 := &mockEqualer{
		Value:    5,
		EqualRet: false,
	}
	e2 := &mockEqualer{Value: 5}

	result := IsEqual(e1, e2)

	if result {
		t.Error("IsEqual returned true")
	}
	if e1.Other != e2 {
		t.Errorf("IsEqual called with %#v instead of %#v", e1.Other, e2)
	}
}

func TestNothingExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(Nothing).Implements(expectIface) {
		t.Error("NothingExpectation must implement Expectation")
	}
}

func TestNothingExpectationCheck(t *testing.T) {
	err := Nothing.Check(42)
	if err != ErrExpectationFailed { //nolint:goerr113
		t.Errorf("Function returned unexpected error %s", err)
	}
}
