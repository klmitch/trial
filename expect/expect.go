// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"reflect"

	"gitlab.com/klmitch/trial/util"
)

// Expectation is the core type for the trial library.  An Expectation
// is an interface of one method, which takes the actual value and
// returns an error if the value does not meet the expectation.
type Expectation interface {
	// Check is a method that checks to see if the expectation is
	// met by a given actual value.  It must return an error if
	// the expectation is not met; the error message will become
	// the default message for the assertion failure.
	Check(actual interface{}) error
}

// Locatable is an interface that may additionally be implemented by
// an [Expectation]; it contains a [Locatable.Location] method that
// returns the [util.Location] where the [Expectation] was created.
type Locatable interface {
	// Location returns the [util.Location] corresponding to the code
	// location which created the [Expectation].
	Location() util.Location
}

// Equaler is an interface that certain expectations, such as
// FuncExpectation, may implement to augment how expectation
// comparison works.
type Equaler interface {
	// Equal compares this Expectation to another, returning true
	// if they are equivalent.
	Equal(other Expectation) bool
}

// IsEqual compares two expectations, returning true if they are equal
// expectations.
func IsEqual(e1, e2 Expectation) bool {
	// If e1 implements Equaler, use its Equal method
	if eq, ok := e1.(Equaler); ok {
		return eq.Equal(e2)
	}

	return reflect.DeepEqual(e1, e2)
}

// FuncExpectation is an implementation of Expectation that invokes a
// function closure to perform the check.
type FuncExpectation struct {
	Func func(actual interface{}) error // The check function
	From util.Location                  // Where the expectation was created
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (fe *FuncExpectation) Check(actual interface{}) error {
	return WithLoc(fe, fe.Func(actual))
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (fe *FuncExpectation) Location() util.Location {
	return fe.From
}

// Equal compares this Expectation to another, returning true if they
// are equivalent.
func (fe *FuncExpectation) Equal(other Expectation) bool {
	// Is other a FuncExpectation?
	fe2, ok := other.(*FuncExpectation)
	if !ok {
		return false
	}

	return fe == fe2
}

// Func is a function that wraps a user-provided check function to
// produce an Expectation that may be passed as an expected argument
// of a Mock or to the AssertThat function.  The function is passed
// the actual value, and must return an error if the expectation is
// not met by that value.  The error message becomes the default
// assertion failure message.
func Func(expect func(actual interface{}) error) *FuncExpectation {
	return &FuncExpectation{
		Func: expect,
		From: util.CalledFrom(0),
	}
}

// AnythingExpectation is an implementation of Expectation that always
// returns no error, indicating that any value is acceptable.
type AnythingExpectation struct{}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (ae AnythingExpectation) Check(_ interface{}) error {
	return nil
}

// Anything is an Expectation that accepts any value.  Its intended
// use is with mocks, as passing it to AssertThat would not make any
// sense.
var Anything = AnythingExpectation{}

// NothingExpectation is an implementation of Expectation that always
// returns an error, indicating that no value is acceptable.
type NothingExpectation struct{}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (ne NothingExpectation) Check(_ interface{}) error {
	return ErrExpectationFailed
}

// Nothing is an Expectation that accepts any value.  Its intended
// use is with mocks, as passing it to AssertThat would not make any
// sense.
var Nothing = NothingExpectation{}
