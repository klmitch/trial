// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"reflect"
	"testing"

	"gitlab.com/klmitch/trial/util"
)

type Unwrapper interface {
	Unwrap() error
}

var (
	errIface       = reflect.TypeOf((*error)(nil)).Elem()
	unwrapperIface = reflect.TypeOf((*Unwrapper)(nil)).Elem()
)

func TestExpectationErrorImplementsErrorAndUnwrapper(t *testing.T) {
	if !reflect.TypeOf(ExpectationError{}).Implements(errIface) {
		t.Error("ExpectationError must implement error")
	}
	if !reflect.TypeOf(ExpectationError{}).Implements(unwrapperIface) {
		t.Error("ExpectationError must implement Unwrapper")
	}
}

func TestExpectationErrorUnwrap(t *testing.T) {
	obj := ExpectationError{}

	result := obj.Unwrap()

	if result != ErrExpectationFailed { //nolint:goerr113
		t.Errorf("Unwrap returned %#v instead of %#v", result, ErrExpectationFailed)
	}
}

func TestExpectationErrorError(t *testing.T) {
	obj := ExpectationError{Actual: 42}

	result := obj.Error()

	if result != "expectation failed; actual value: 42" {
		t.Errorf("Error returned %q instead of \"expectation failed; actual value: 42\"", result)
	}
}

func TestLocatableErrorImplementsErrorAndUnwrapper(t *testing.T) {
	if !reflect.TypeOf(LocatableError{}).Implements(errIface) {
		t.Error("LocatableError must implement error")
	}
	if !reflect.TypeOf(LocatableError{}).Implements(unwrapperIface) {
		t.Error("LocatableError must implement Unwrapper")
	}
}

func TestLocatableErrorUnwrap(t *testing.T) {
	obj := LocatableError{
		Err: ErrExpectationFailed,
	}

	result := obj.Unwrap()

	if result != ErrExpectationFailed { //nolint:goerr113
		t.Errorf("Unwrap returned %#v instead of %#v", result, ErrExpectationFailed)
	}
}

func TestLocatableErrorError(t *testing.T) {
	obj := LocatableError{
		Err: ErrExpectationFailed,
	}

	result := obj.Error()

	if result != ErrExpectationFailed.Error() {
		t.Errorf("Error returned %q instead of %q", result, ErrExpectationFailed.Error())
	}
}

func TestLocationBase(t *testing.T) {
	loc := util.Location{
		File: "/some/file",
		Line: 42,
		Func: "pkg.func",
	}
	err := LocatableError{
		Location: loc,
		Err:      ErrExpectationFailed,
	}

	result, ok := Location(err)

	if !ok {
		t.Error("Location returned not ok")
	}
	if result != loc {
		t.Errorf("Location returned %#v instead of %#v", result, loc)
	}
}

func TestLocationUnlocatable(t *testing.T) {
	err := ErrExpectationFailed

	result, ok := Location(err)

	if ok {
		t.Error("Location returned ok")
	}
	if result != (util.Location{}) {
		t.Errorf("Location returned %#v instead of %#v", result, util.Location{})
	}
}

func TestWithLocBase(t *testing.T) {
	e := mockLocatable{
		Loc: util.Location{
			Line: 42,
		},
	}
	err := AnError

	result := WithLoc(e, err)

	expected := LocatableError{
		Location: util.Location{
			Line: 42,
		},
		Err: AnError,
	}
	if result != expected { //nolint:goerr113
		t.Errorf("WithLoc returned %#v instead of %#v", result, expected)
	}
}

func TestWithLocNoError(t *testing.T) {
	e := mockLocatable{
		Loc: util.Location{
			Line: 42,
		},
	}

	result := WithLoc(e, nil)

	if result != nil { //nolint:goerr113
		t.Errorf("WithLoc returned %#v instead of nil", result)
	}
}

func TestWithLocUnlocatable(t *testing.T) {
	e := mockExpectation{}
	err := AnError

	result := WithLoc(e, err)

	if result != AnError { //nolint:goerr113
		t.Errorf("WithLoc returned %#v instead of AnError", result)
	}
}
