// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"errors"
	"fmt"

	"gitlab.com/klmitch/trial/util"
)

// NotOption is an option for Not.
type NotOption interface {
	// Apply applies the option to a NotExpectation.
	Apply(ne *NotExpectation)
}

// NotErrorOption is an option that specifies an error to return when the
// wrapped expectation is met.
type NotErrorOption struct {
	Error error // The fixed error to return
}

// Apply applies the option to a NotExpectation.
func (o NotErrorOption) Apply(ne *NotExpectation) {
	ne.Error = o.Error
}

// NotError returns a Not option that specifies a fixed error to
// return if the expectation is not met.
func NotError(err error) NotErrorOption {
	return NotErrorOption{
		Error: err,
	}
}

// NotFactoryOption is an option that specifies a factory to use to
// construct an error on behalf of NotExpectation.
type NotFactoryOption struct {
	Factory func(actual interface{}) error // The factory function to use
}

// Apply applies the option to a NotExpectation.
func (o NotFactoryOption) Apply(ne *NotExpectation) {
	ne.Factory = o.Factory
}

// NotFactory returns a Not option that specifies an error factory to
// use to construct an error to return.  The factory will be passed
// the actual value used in the Check.
func NotFactory(factory func(actual interface{}) error) NotFactoryOption {
	return NotFactoryOption{
		Factory: factory,
	}
}

// NotExpectation is an implementation of Expectation that inverts an
// expectation; that is, it returns a (rather generic) error if the
// wrapped expectation doesn't return one.
type NotExpectation struct {
	Wrapped Expectation                    // The Expectation to invert
	Error   error                          // The error to return if not met
	Factory func(actual interface{}) error // Factory function to create error
	From    util.Location                  // Where the expectation was created
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (ne NotExpectation) Check(actual interface{}) error {
	err := ne.Wrapped.Check(actual)
	if err == nil {
		// Use the factory if one is set
		if ne.Factory != nil {
			return WithLoc(ne, ne.Factory(actual))
		}

		return WithLoc(ne, ne.Error)
	}

	return nil
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (ne NotExpectation) Location() util.Location {
	return ne.From
}

// Not is a function that wraps another Expectation in a
// NotExpectation, to invert the sense.  This is provided to allow
// inverting an expectation when no inverted expectation exists.  By
// default, returns an ErrExpectationFailed, but another static error
// can be specified by passing the NotError option, or an error
// factory can be used by passing the NotFactory option.
func Not(e Expectation, opts ...NotOption) NotExpectation {
	// Pick the location
	var loc util.Location
	if l, ok := e.(Locatable); ok {
		loc = l.Location()
	} else {
		loc = util.CalledFrom(0)
	}

	obj := NotExpectation{
		Wrapped: e,
		Error:   ErrExpectationFailed,
		From:    loc,
	}

	// Apply options
	for _, opt := range opts {
		opt.Apply(&obj)
	}

	return obj
}

// AndExpectation is an implementation of Expectation that combines
// several Expectation implementations, requiring that all of them are
// met.
type AndExpectation struct {
	Wrapped []Expectation // The Expectation instances to combine
	From    util.Location // Where the expectation was created
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (ae AndExpectation) Check(actual interface{}) error {
	for _, e := range ae.Wrapped {
		err := e.Check(actual)
		if err != nil {
			// Ensure a location is attached to the error
			var le LocatableError
			if !errors.As(err, &le) {
				return WithLoc(ae, err)
			}
			return err
		}
	}

	return nil
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (ae AndExpectation) Location() util.Location {
	return ae.From
}

// And is a function that wraps other Expectation instances in an
// AndExpectation, to require that multiple expectations must be met.
func And(e ...Expectation) AndExpectation {
	return AndExpectation{
		Wrapped: e,
		From:    util.CalledFrom(0),
	}
}

// OrExpectation is an implementation of Expectation that combines
// several Expectation implementations, requiring that at least one of
// them are met.
type OrExpectation struct {
	Wrapped []Expectation // The Expectation instances to combine
	From    util.Location // Where the expectation was created
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (oe OrExpectation) Check(actual interface{}) error {
	var savedErr error
	for _, e := range oe.Wrapped {
		err := e.Check(actual)
		if err == nil {
			return nil
		} else if savedErr == nil {
			savedErr = err
		}
	}

	// Ensure a location is attached to the error
	var le LocatableError
	if !errors.As(savedErr, &le) {
		return WithLoc(oe, savedErr)
	}
	return savedErr
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (oe OrExpectation) Location() util.Location {
	return oe.From
}

// Or is a function that wraps other Expectation instances in an
// OrExpectation, to require that multiple expectations must be met.
func Or(e ...Expectation) OrExpectation {
	return OrExpectation{
		Wrapped: e,
		From:    util.CalledFrom(0),
	}
}

// XorError is an error returned by the XorExpectation's Check method.
// It accumulates the list of all met expectations.
type XorError struct {
	ExpectationError

	Met []Expectation // The list of met expectations
}

// Error returns the error message.
func (e XorError) Error() string {
	return fmt.Sprintf("%s; actual value: %#v; %d expectations met", ErrExpectationFailed, e.Actual, len(e.Met))
}

// XorExpectation is an implementation of Expectation that combines
// several Expectation implementations, requiring that at least one of
// them are met.
type XorExpectation struct {
	Wrapped []Expectation // The Expectation instances to combine
	From    util.Location // Where the expectation was created
}

// Check is a method that checks to see if the expectation is met by a
// given actual value.  It must return an error if the expectation is
// not met; the error message will become the default message for the
// assertion failure.
func (xe XorExpectation) Check(actual interface{}) error {
	var savedErr error
	metErr := XorError{
		ExpectationError: ExpectationError{
			Actual: actual,
		},
	}
	pass := false
	fail := false
	for _, e := range xe.Wrapped {
		err := e.Check(actual)
		if err == nil {
			metErr.Met = append(metErr.Met, e)
			if pass {
				savedErr = metErr
				fail = true
			}
			pass = true
		} else if savedErr == nil {
			savedErr = err
		}
	}

	// Return the correct error
	if fail || !pass {
		// Ensure a location is attached to the error
		var le LocatableError
		if !errors.As(savedErr, &le) {
			return WithLoc(xe, savedErr)
		}
		return savedErr
	}

	return nil
}

// Location returns the [util.Location] corresponding to the code
// location which created the [Expectation].
func (xe XorExpectation) Location() util.Location {
	return xe.From
}

// Xor is a function that wraps other Expectation instances in an
// XorExpectation, to require that multiple expectations must be met.
func Xor(e ...Expectation) XorExpectation {
	return XorExpectation{
		Wrapped: e,
		From:    util.CalledFrom(0),
	}
}
