// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"bytes"
	"reflect"
	"testing"

	"gitlab.com/klmitch/trial/util"
)

func TestEqualErrorImplementsErrorAndUnwrapper(t *testing.T) {
	if !reflect.TypeOf(EqualError{}).Implements(errIface) {
		t.Error("EqualError must implement error")
	}
	if !reflect.TypeOf(EqualError{}).Implements(unwrapperIface) {
		t.Error("EqualError must implement Unwrapper")
	}
}

func TestEqualErrorError(t *testing.T) {
	obj := EqualError{
		ExpectationError: ExpectationError{
			Actual: 42,
		},
		Expected: 37,
	}

	result := obj.Error()

	expected := "expectation failed; expected 37, actual value: 42"
	if result != expected {
		t.Errorf("Error returned %q instead of %q", result, expected)
	}
}

func TestEqualExpectationImplementsExpectation(t *testing.T) {
	if !reflect.TypeOf(EqualExpectation{}).Implements(expectIface) {
		t.Error("EqualExpectation must implement Expectation")
	}
	if !reflect.TypeOf(EqualExpectation{}).Implements(locatableIface) {
		t.Error("EqualExpectation must implement Locatable")
	}
}

func TestEqualExpectationMkErr(t *testing.T) {
	obj := EqualExpectation{
		Expected: 42,
	}

	result := obj.mkErr(37)

	le, ok := result.(LocatableError)
	if !ok {
		t.Error("mkErr did not return LocatableError")
	} else {
		if le.Err.(EqualError).Actual != 37 {
			t.Errorf("mkErr set Actual to %#v instead of 37", result.(EqualError).Actual)
		}
		if le.Err.(EqualError).Expected != 42 {
			t.Errorf("mkErr set Expected to %#v instead of 42", result.(EqualError).Expected)
		}
	}
}

func TestEqualExpectationCheckBase(t *testing.T) {
	obj := EqualExpectation{
		Expected: 42,
	}

	err := obj.Check(42)
	if err != nil {
		t.Errorf("Check returned unexpected error %#v", err)
	}
}

func TestEqualExpectationCheckNotEqual(t *testing.T) {
	obj := EqualExpectation{
		Expected: 42,
	}

	err := obj.Check(37)
	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(EqualError)
			if !ok || actErr.Actual != 37 || actErr.Expected != 42 {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestEqualExpectationCheckByteArrayBase(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte("test"),
	}

	err := obj.Check([]byte("test"))
	if err != nil {
		t.Errorf("Check returned unexpected error %#v", err)
	}
}

func TestEqualExpectationCheckByteArrayNotEqual(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte("test"),
	}

	err := obj.Check([]byte("tset"))

	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(EqualError)
			if !ok || !bytes.Equal(actErr.Actual.([]byte), []byte("tset")) || !bytes.Equal(actErr.Expected.([]byte), []byte("test")) {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestEqualExpectationCheckByteArrayBothNil(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte(nil),
	}

	err := obj.Check([]byte(nil))
	if err != nil {
		t.Errorf("Check returned unexpected error %#v", err)
	}
}

func TestEqualExpectationCheckByteArrayExpectedNil(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte(nil),
	}

	err := obj.Check([]byte("tset"))

	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(EqualError)
			if !ok || !bytes.Equal(actErr.Actual.([]byte), []byte("tset")) || actErr.Expected.([]byte) != nil {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestEqualExpectationCheckByteArrayActualNil(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte("test"),
	}

	err := obj.Check([]byte(nil))

	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(EqualError)
			if !ok || actErr.Actual.([]byte) != nil || !bytes.Equal(actErr.Expected.([]byte), []byte("test")) {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestEqualExpectationCheckByteArrayNotSameType(t *testing.T) {
	obj := EqualExpectation{
		Expected: []byte("test"),
	}

	err := obj.Check(42)

	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(EqualError)
			if !ok || actErr.Actual != 42 || !bytes.Equal(actErr.Expected.([]byte), []byte("test")) {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestEqualExpectationCheckBothNil(t *testing.T) {
	obj := EqualExpectation{
		Expected: nil,
	}

	err := obj.Check(nil)
	if err != nil {
		t.Errorf("Check returned unexpected error %#v", err)
	}
}

func TestEqualExpectationCheckFunction(t *testing.T) {
	f := func() {}
	obj := EqualExpectation{
		Expected: f,
	}

	err := obj.Check(f)

	if err == nil {
		t.Error("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		}
		if _, ok = le.Err.(EqualError); !ok {
			t.Errorf("Check returned incorrect error: %#v", err)
		}
	}
}

func TestEqualExpectationLocation(t *testing.T) {
	loc := util.Location{
		Line: 23,
	}
	obj := EqualExpectation{
		From: loc,
	}

	result := obj.Location()

	if result != loc {
		t.Errorf("Location returned %#v, expected %#v", result, loc)
	}
}

func TestEqual(t *testing.T) {
	result := Equal(42)

	if result.Expected != 42 {
		t.Errorf("Equal returned incorrect %#v", result)
	}
}

func TestNotEqualErrorImplementsErrorAndUnwrapper(t *testing.T) {
	if !reflect.TypeOf(NotEqualError{}).Implements(errIface) {
		t.Error("NotEqualError must implement error")
	}
	if !reflect.TypeOf(NotEqualError{}).Implements(unwrapperIface) {
		t.Error("NotEqualError must implement Unwrapper")
	}
}

func TestNotEqualErrorError(t *testing.T) {
	obj := NotEqualError{
		ExpectationError: ExpectationError{
			Actual: 42,
		},
		Expected: 37,
	}

	result := obj.Error()

	expected := "expectation failed; expected not equal to 37, actual value: 42"
	if result != expected {
		t.Errorf("Error returned %q instead of %q", result, expected)
	}
}

func TestNotEqualCheckEqual(t *testing.T) {
	obj := NotEqual(42)

	err := obj.Check(42)

	if err == nil {
		t.Errorf("Check failed to return an error")
	} else {
		le, ok := err.(LocatableError)
		if !ok {
			t.Error("Check returned non-locatable error")
		} else {
			actErr, ok := le.Err.(NotEqualError)
			if !ok || actErr.Actual != 42 || actErr.Expected != 42 {
				t.Errorf("Check returned incorrect error: %#v", err)
			}
		}
	}
}

func TestNotEqualCheckNotEqual(t *testing.T) {
	obj := NotEqual(42)

	err := obj.Check(37)
	if err != nil {
		t.Errorf("Check returned unexpected error %#v", err)
	}
}
