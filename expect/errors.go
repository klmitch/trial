// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import (
	"errors"
	"fmt"

	"gitlab.com/klmitch/trial/util"
)

// Expectation failure error.  This is the most generic expectation
// failure error possible.
var ErrExpectationFailed = errors.New("expectation failed")

// ExpectationError is a base error for all expectation errors.  It
// implements an Unwrap method that returns [ErrExpectationFailed], as
// well as including the actual value passed to the expectation's
// [Expectation.Check] method.
type ExpectationError struct {
	Actual interface{} // Actual value
}

// Unwrap is called to unwrap the underlying error.  An
// [ExpectationError] is an extension of [ErrExpectationFailed], so this
// implementation simply returns that value.
func (e ExpectationError) Unwrap() error {
	return ErrExpectationFailed
}

// Error returns the base error message.  Extensions are expected to
// override this method.
func (e ExpectationError) Error() string {
	return fmt.Sprintf("%s; actual value: %#v", ErrExpectationFailed, e.Actual)
}

// LocatableError is a wrapper for an [ExpectationError] which also
// includes the [util.Location] where the [Expectation] was created.
type LocatableError struct {
	Location util.Location // Location of the expectation
	Err      error         // The wrapped error
}

// Unwrap is called to unwrap the underlying error.
func (e LocatableError) Unwrap() error {
	return e.Err
}

// Error returns the base error message.  For a [LocatableError], this
// returns the error message of the wrapped error.
func (e LocatableError) Error() string {
	return e.Err.Error()
}

// Location returns the [util.Location] associated with the first
// [LocatableError] in the error chain.  If no [LocatableError] is
// present in the error chain, the boolean return value will be false.
func Location(err error) (util.Location, bool) {
	var le LocatableError
	if errors.As(err, &le) {
		return le.Location, true
	}

	return util.Location{}, false
}

// WithLoc is a utility function that takes the expectation and an
// error and returns that error wrapped with [LocatableError] if the
// expectation implements the [Locatable] interface.
func WithLoc(e Expectation, err error) error {
	// Is there an error to wrap?
	if err == nil {
		return nil
	}

	// If expectation is locatable, wrap the error
	if l, ok := e.(Locatable); ok {
		return LocatableError{
			Location: l.Location(),
			Err:      err,
		}
	}

	return err
}
