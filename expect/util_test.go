// Copyright (c) 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package expect

import "testing"

func TestIsFunctionBase(t *testing.T) {
	result := isFunction(42)

	if result {
		t.Error("isFunction returned true")
	}
}

func TestIsFunctionNil(t *testing.T) {
	result := isFunction(nil)

	if result {
		t.Error("isFunction returned true")
	}
}

func TestIsFunctionFunction(t *testing.T) {
	result := isFunction(func() {})

	if !result {
		t.Error("isFunction returned false")
	}
}
