// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import "testing"

func tryRecover(f func()) (result interface{}) {
	defer func() {
		if panicData := recover(); panicData != nil {
			result = panicData
		}
	}()
	f()
	return nil
}

type mockPatcher struct {
	Applied  bool
	Restored bool
}

func (m *mockPatcher) Apply() {
	m.Applied = true
}

func (m *mockPatcher) Restore() {
	m.Restored = true
}

func TestApply(t *testing.T) {
	p := &mockPatcher{}
	theT := &mockT{}

	Apply(theT, p)

	if !theT.HelperCalled {
		t.Error("Patch failed to set Helper")
	}
	if !p.Applied {
		t.Error("Patch failed to apply patch")
	}
	if p.Restored {
		t.Error("Patch prematurely restored patch")
	}
	if len(theT.Cleanups) != 1 {
		t.Error("Patch failed to set cleanup")
	} else {
		theT.Cleanups[0]()
		if !p.Restored {
			t.Error("Patch failed to set correct cleanup")
		}
	}
}
