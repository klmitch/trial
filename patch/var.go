// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"fmt"
	"reflect"
)

// VarPatcher is a patcher that, given a pointer to a variable and the
// desired patch value, will set that variable to that value.
type VarPatcher struct {
	Variable reflect.Value // The variable to patch
	Value    reflect.Value // The value to set it to
	Original reflect.Value // The variable's original value
	Applied  bool          // Indicates the patch is applied
}

// SetVar constructs and applies a VarPatcher, setting the desired
// value of the variable and returning the VarPatcher.  It could be
// used in a test function like so:
//
//	func TestDoSomething(t *testing.T) {
//		patch.SetVar(t, &readFile, func(filename string) ([]byte, error) {
//			return []byte("hello"), nil
//		}
//
//		err := DoSomething("some-filename")
//
//		assert.NoError(t, err)
//	}
func SetVar(t T, variable, value interface{}) *VarPatcher {
	// We're a helper function
	t.Helper()

	// Select the variable and validate it's a settable object
	varReflect := reflect.ValueOf(variable)
	if varReflect.Type().Kind() != reflect.Ptr {
		panic("cannot set variable passed to SetVar")
	}
	v := varReflect.Elem()

	// Convert the desired value and check that it can be assigned
	// to the variable
	val := reflect.ValueOf(value)
	if !val.Type().AssignableTo(v.Type()) {
		panic(fmt.Sprintf("cannot assign %s type to variable type %s", val.Type(), v.Type()))
	}

	// Construct the patcher
	p := &VarPatcher{
		Variable: v,
		Value:    val,
	}

	// Apply it
	apply(t, p)

	return p
}

// Apply applies the patch.  It is assumed that the Restore method
// will have sufficient data to restore the patch.
func (vp *VarPatcher) Apply() {
	// Be idempotent
	if vp.Applied {
		return
	}

	// Save the current value of the variable
	vp.Original = reflect.ValueOf(vp.Variable.Interface())

	// Set the variable to the desired value
	vp.Variable.Set(vp.Value)
	vp.Applied = true
}

// Restore restores the patch to its original setting.  It must be
// idempotent: if called a second time, there must be no effect.
func (vp *VarPatcher) Restore() {
	// Be idempotent
	if !vp.Applied {
		return
	}

	// Restore the variable to the original value
	vp.Variable.Set(vp.Original)
	vp.Applied = false
}
