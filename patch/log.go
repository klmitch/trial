// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"bytes"
	"io"
	"log"
)

// LogPatcher is a patcher that overrides the output of the default
// logger from the log package to point to itself.  A LogPatcher
// embeds a bytes.Buffer, so any bytes.Buffer method can be used with
// it.
type LogPatcher struct {
	bytes.Buffer
	Original io.Writer // The original value of the log output
	Applied  bool      // Indicates the patch is applied
}

// Patch points for testing the units in this file.
var (
	logWriter    = log.Writer
	logSetOutput = log.SetOutput
)

// SetLog constructs and applies a LogPatcher.  It could be used in a
// test function like so:
//
//	func TestDoSomething(t *testing.T) {
//		logOut := patch.SetLog(t)
//
//		DoSomething("some argument")
//
//		assert.Equal(t, "", logOut.String())
//	}
func SetLog(t T) *LogPatcher {
	// We're a helper function
	t.Helper()

	// Construct the patcher
	p := &LogPatcher{}

	// Apply it
	apply(t, p)

	return p
}

// Apply applies the patch.  It is assumed that the Restore method
// will have sufficient data to restore the patch.
func (lp *LogPatcher) Apply() {
	// Be idempotent
	if lp.Applied {
		return
	}

	// Save the current value of the output
	lp.Original = logWriter()

	// Set the patch value
	logSetOutput(lp)
	lp.Applied = true
}

// Restore restores the patch to its original setting.  It must be
// idempotent: if called a second time, there must be no effect.
func (lp *LogPatcher) Restore() {
	// Be idempotent
	if !lp.Applied {
		return
	}

	// Restore the log output to the original value
	logSetOutput(lp.Original)
	lp.Applied = false
}
