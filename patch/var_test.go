// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

//nolint:goconst
package patch

import (
	"reflect"
	"testing"
)

func TestVarPatcherImplementsPatcher(t *testing.T) {
	iface := reflect.TypeOf((*Patcher)(nil)).Elem()

	if !reflect.TypeOf(&VarPatcher{}).Implements(iface) {
		t.Error("VarPatcher must implement Patcher")
	}
}

func TestSetVarBase(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})
	theVar := "unpatched"

	result := SetVar(theT, &theVar, "patched")

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if result.Variable != reflect.ValueOf(&theVar).Elem() {
		t.Error("failed to store proper variable")
	}
	if result.Value != reflect.ValueOf("patched") {
		t.Error("failed to store proper value")
	}
	if !applyCalled {
		t.Error("Apply not called")
	}
}

func TestSetVarUnsettable(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})
	theVar := "unpatched"

	panicData := tryRecover(func() { SetVar(theT, theVar, "patched") })

	if panicData == nil {
		t.Error("SetVar failed to panic")
	} else if panicData != "cannot set variable passed to SetVar" {
		t.Errorf("SetVar paniced for the wrong reason: %+v", panicData)
	}
	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if applyCalled {
		t.Error("Apply called")
	}
}

func TestSetVarUnassignable(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})
	theVar := "unpatched"

	panicData := tryRecover(func() { SetVar(theT, &theVar, 12345) })

	if panicData == nil {
		t.Error("SetVar failed to panic")
	} else if panicData != "cannot assign int type to variable type string" {
		t.Errorf("SetVar paniced for the wrong reason: %+v", panicData)
	}
	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if applyCalled {
		t.Error("Apply called")
	}
}

func TestVarPatcherApplyBase(t *testing.T) {
	theVar := "unpatched"
	obj := &VarPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Value:    reflect.ValueOf("patched"),
	}

	obj.Apply()

	if theVar != "patched" {
		t.Errorf("theVar should be \"patched\", is actually %q", theVar)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
}

func TestVarPatcherApplyIdempotent(t *testing.T) {
	theVar := "unpatched"
	obj := &VarPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Value:    reflect.ValueOf("patched"),
		Applied:  true,
	}

	obj.Apply()

	if theVar != "unpatched" {
		t.Errorf("theVar should be \"unpatched\", is actually %q", theVar)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
}

func TestVarPatcherRestoreBase(t *testing.T) {
	theVar := "patched"
	obj := &VarPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Original: reflect.ValueOf("unpatched"),
		Applied:  true,
	}

	obj.Restore()

	if theVar != "unpatched" {
		t.Errorf("theVar should be \"unpatched\", is actually %q", theVar)
	}
	if obj.Applied {
		t.Error("Applied set")
	}
}

func TestVarPatcherRestoreIdempotent(t *testing.T) {
	theVar := "patched"
	obj := &VarPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Original: reflect.ValueOf("unpatched"),
	}

	obj.Restore()

	if theVar != "patched" {
		t.Errorf("theVar should be \"patched\", is actually %q", theVar)
	}
	if obj.Applied {
		t.Error("Applied set")
	}
}
