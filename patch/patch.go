// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

// Patcher is an interface for patchers.
type Patcher interface {
	// Apply applies the patch.  It is assumed that the Restore
	// method will have sufficient data to restore the patch.
	Apply()

	// Restore restores the patch to its original setting.  It
	// must be idempotent: if called a second time, there must be
	// no effect.
	Restore()
}

// Apply applies a Patcher implementation.  It uses the testing.T
// value to set a cleanup function that will call the Patcher.Restore
// method.
func Apply(t T, p Patcher) {
	// We're a helper function
	t.Helper()

	// Apply the patch
	p.Apply()

	// Schedule the cleanup
	t.Cleanup(p.Restore)
}

// Patch points for testing the units in this package.
var (
	apply = Apply
)
