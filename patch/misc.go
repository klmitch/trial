// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

// T is an interface matching the elements of testing.T that we
// require.
type T interface {
	// Cleanup registers a function to be called when the test and
	// all its subtests complete.
	Cleanup(f func())

	// Helper marks the calling function as a test helper
	// function.
	Helper()
}
