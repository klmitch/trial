// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import "os"

// EnvPatcher is a patcher that, given an environment variable name,
// will set or unset that environment variable.
type EnvPatcher struct {
	Name     string  // Name of the variable
	Value    *string // Desired value; nil indicates unset
	Original *string // Original value; nil indicates unset
	Applied  bool    // Indicates the patch is applied
}

// Patch points for testing the units in this file.
var (
	setenv      = os.Setenv
	lookupenv   = os.LookupEnv
	unsetenv    = os.Unsetenv
	setEnvPatch = setEnv
)

// setEnv is a helper for the EnvPatcher that sets or unsets an
// environment variable depending on whether the value pointer is nil
// or a string.  It will panic if there's an error.
func setEnv(name string, value *string) {
	var err error
	if value == nil {
		// Only unset if it's actually set
		if _, ok := lookupenv(name); ok {
			err = unsetenv(name)
		}
	} else {
		err = setenv(name, *value)
	}

	// If there was an error setting or unsetting the variable,
	// panic
	if err != nil {
		panic(err)
	}
}

// SetEnv constructs and applies an EnvPatcher, storing the desired
// value of the specified environment variable and returning the
// EnvPatcher.  It could be used in a test function like so:
//
//	func TestDoSomething(t *testing.T) {
//		patch.SetEnv(t, "VARNAME", "value")
//
//		err := DoSomething("some-filename")
//
//		assert.NoError(t, err)
//	}
func SetEnv(t T, name, value string) *EnvPatcher {
	// We're a helper function
	t.Helper()

	p := &EnvPatcher{
		Name:  name,
		Value: &value,
	}

	// Apply the patcher
	apply(t, p)

	return p
}

// UnsetEnv constructs and applies an EnvPatcher, storing that the
// specified environment variable should be unset, and returning the
// EnvPatcher.  It could be used in a test function like so:
//
//	func TestDoSomething(t *testing.T) {
//		patch.UnsetEnv(t, "VARNAME")
//
//		err := DoSomething()
//
//		assert.NoError(t, err)
//	}
func UnsetEnv(t T, name string) *EnvPatcher {
	// We're a helper function
	t.Helper()

	p := &EnvPatcher{
		Name: name,
	}

	// Apply the patcher
	apply(t, p)

	return p
}

// Apply applies the patch.  It is assumed that the Restore method
// will have sufficient data to restore the patch.
func (ep *EnvPatcher) Apply() {
	// Be idempotent
	if ep.Applied {
		return
	}

	// Save the current value of the environment variable
	if value, ok := lookupenv(ep.Name); ok {
		ep.Original = &value
	} else {
		ep.Original = nil
	}

	// Set the environment variable to the desired value
	setEnvPatch(ep.Name, ep.Value)
	ep.Applied = true
}

// Restore restores the patch to its original setting.  It must be
// idempotent: if called a second time, there must be no effect.
func (ep *EnvPatcher) Restore() {
	// Be idempotent
	if !ep.Applied {
		return
	}

	// Restore the environment variable to the original value
	setEnvPatch(ep.Name, ep.Original)
	ep.Applied = false
}
