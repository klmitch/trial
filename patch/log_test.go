// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"bytes"
	"io"
	"reflect"
	"testing"
)

func TestLogPatcherImplementsPatcher(t *testing.T) {
	iface := reflect.TypeOf((*Patcher)(nil)).Elem()

	if !reflect.TypeOf(&LogPatcher{}).Implements(iface) {
		t.Error("LogPatcher must implement Patcher")
	}
}

func TestSetLog(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})

	result := SetLog(theT)

	if result == nil {
		t.Error("SetLog failed to return a Patcher")
	}
	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if !applyCalled {
		t.Error("Apply not called")
	}
}

func TestLogPatcherApplyBase(t *testing.T) {
	orig := &bytes.Buffer{}
	saveLogWriter := logWriter
	saveLogSetOutput := logSetOutput
	logWriterCalled := false
	logSetOutputCalled := false
	logWriter = func() io.Writer {
		logWriterCalled = true
		return orig
	}
	logSetOutput = func(w io.Writer) {
		if w == orig {
			t.Error("log output not set to expected value")
		}
		logSetOutputCalled = true
	}
	t.Cleanup(func() {
		logWriter = saveLogWriter
		logSetOutput = saveLogSetOutput
	})
	obj := &LogPatcher{}

	obj.Apply()

	if obj.Original != orig {
		t.Errorf("expected original %+v, got %+v", orig, obj.Original)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
	if !logWriterCalled {
		t.Error("logWriter not called")
	}
	if !logSetOutputCalled {
		t.Error("logSetOutput not called")
	}
}

func TestLogPatcherApplyIdempotent(t *testing.T) {
	orig := &bytes.Buffer{}
	saveLogWriter := logWriter
	saveLogSetOutput := logSetOutput
	logWriterCalled := false
	logSetOutputCalled := false
	logWriter = func() io.Writer {
		logWriterCalled = true
		return orig
	}
	logSetOutput = func(_ io.Writer) {
		logSetOutputCalled = true
	}
	t.Cleanup(func() {
		logWriter = saveLogWriter
		logSetOutput = saveLogSetOutput
	})
	obj := &LogPatcher{
		Applied: true,
	}

	obj.Apply()

	if obj.Original != nil {
		t.Errorf("expected original nil, got %+v", obj.Original)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
	if logWriterCalled {
		t.Error("logWriter called")
	}
	if logSetOutputCalled {
		t.Error("logSetOutput called")
	}
}

func TestLogPatcherRestoreBase(t *testing.T) {
	orig := &bytes.Buffer{}
	saveLogSetOutput := logSetOutput
	logSetOutputCalled := false
	logSetOutput = func(w io.Writer) {
		if w != orig {
			t.Error("original log output not provided")
		}
		logSetOutputCalled = true
	}
	t.Cleanup(func() {
		logSetOutput = saveLogSetOutput
	})
	obj := &LogPatcher{
		Original: orig,
		Applied:  true,
	}

	obj.Restore()

	if obj.Applied {
		t.Error("Applied set")
	}
	if !logSetOutputCalled {
		t.Error("logSetOutput not called")
	}
}

func TestLogPatcherRestoreIdempotent(t *testing.T) {
	orig := &bytes.Buffer{}
	saveLogSetOutput := logSetOutput
	logSetOutputCalled := false
	logSetOutput = func(_ io.Writer) {
		logSetOutputCalled = true
	}
	t.Cleanup(func() {
		logSetOutput = saveLogSetOutput
	})
	obj := &LogPatcher{
		Original: orig,
		Applied:  false,
	}

	obj.Restore()

	if obj.Applied {
		t.Error("Applied set")
	}
	if logSetOutputCalled {
		t.Error("logSetOutput called")
	}
}
