// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"io"
	"os"
	"reflect"
	"testing"
)

func TestStreamPatcherImplementsPatcher(t *testing.T) {
	iface := reflect.TypeOf((*Patcher)(nil)).Elem()

	if !reflect.TypeOf(&StreamPatcher{}).Implements(iface) {
		t.Error("StreamPatcher must implement Patcher")
	}
}

func TestSetStreamBase(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})
	var theVar io.Reader = os.Stdout

	result := SetStream(theT, &theVar)

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if result.Variable != reflect.ValueOf(&theVar).Elem() {
		t.Error("Failed to store proper variable")
	}
	if !applyCalled {
		t.Error("Apply not called")
	}
}

func TestSetStreamUnsettable(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})
	theVar := "unsettable"

	panicData := tryRecover(func() { SetStream(theT, theVar) })

	if panicData == nil {
		t.Error("SetStream failed to panic")
	} else if panicData != "cannot set variable passed to SetStream" {
		t.Errorf("SetStream paniced for the wrong reason: %+v", panicData)
	}
	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if applyCalled {
		t.Error("Apply called")
	}
}

func TestSetStreamUnassignable(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})

	panicData := tryRecover(func() { SetStream(theT, &os.Stdout) })

	if panicData == nil {
		t.Error("SetStream failed to panic")
	} else if panicData != "cannot assign *patch.StreamPatcher type to variable type *os.File" {
		t.Errorf("SetStream paniced for the wrong reason: %+v", panicData)
	}
	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if applyCalled {
		t.Error("Apply called")
	}
}

func TestStreamPatcherApplyBase(t *testing.T) {
	var theVar io.Reader = os.Stdout
	obj := &StreamPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
	}

	obj.Apply()

	if theVar != obj {
		t.Errorf("theVar should be %+v, is actually %+v", obj, theVar)
	}
	if obj.Original != reflect.ValueOf(os.Stdout) {
		t.Errorf("Original should be %+v, is actually %+v", os.Stdout, obj.Original.Interface())
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
}

func TestStreamPatcherApplyIdempotent(t *testing.T) {
	var theVar io.Reader = os.Stdout
	obj := &StreamPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Applied:  true,
	}

	obj.Apply()

	if theVar != os.Stdout {
		t.Errorf("theVar should be %+v, is actually %+v", os.Stdout, theVar)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
}

func TestStreamPatcherRestoreBase(t *testing.T) {
	var theVar io.Reader
	obj := &StreamPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Original: reflect.ValueOf(os.Stdout),
		Applied:  true,
	}
	theVar = obj

	obj.Restore()

	if theVar != os.Stdout {
		t.Errorf("theVar should be %+v, is actually %+v", os.Stdout, theVar)
	}
	if obj.Applied {
		t.Error("Applied set")
	}
}

func TestStreamPatcherRestoreIdempotent(t *testing.T) {
	var theVar io.Reader
	obj := &StreamPatcher{
		Variable: reflect.ValueOf(&theVar).Elem(),
		Original: reflect.ValueOf(os.Stdout),
	}
	theVar = obj

	obj.Restore()

	if theVar != obj {
		t.Errorf("theVar should be %+v, is actually %+v", obj, theVar)
	}
	if obj.Applied {
		t.Error("Applied set")
	}
}
