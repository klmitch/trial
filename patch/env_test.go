// Copyright (c) 2021, 2024 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"reflect"
	"testing"

	"gitlab.com/klmitch/trial/assert"
)

func TestEnvPatcherImplementsPatcher(t *testing.T) {
	iface := reflect.TypeOf((*Patcher)(nil)).Elem()

	if !reflect.TypeOf(&EnvPatcher{}).Implements(iface) {
		t.Error("EnvPatcher must implement Patcher")
	}
}

func TestInternalSetEnvUnsetExists(t *testing.T) {
	saveSetenv := setenv
	saveLookupenv := lookupenv
	saveUnsetenv := unsetenv
	setenvCalled := false
	lookupenvCalled := false
	unsetenvCalled := false
	setenv = func(name, value string) error {
		if name != "ENV" { //nolint:goconst
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if value != "value" { //nolint:goconst
			t.Errorf("expected value \"value\", got %q", value)
		}
		setenvCalled = true
		return nil
	}
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "", true
	}
	unsetenv = func(name string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		unsetenvCalled = true
		return nil
	}
	t.Cleanup(func() {
		setenv = saveSetenv
		lookupenv = saveLookupenv
		unsetenv = saveUnsetenv
	})

	setEnv("ENV", nil)

	if setenvCalled {
		t.Error("Setenv called")
	}
	if !lookupenvCalled {
		t.Error("LookupEnv not called")
	}
	if !unsetenvCalled {
		t.Error("Unsetenv not called")
	}
}

func TestInternalSetEnvUnsetMissing(t *testing.T) {
	saveSetenv := setenv
	saveLookupenv := lookupenv
	saveUnsetenv := unsetenv
	setenvCalled := false
	lookupenvCalled := false
	unsetenvCalled := false
	setenv = func(name, value string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if value != "value" {
			t.Errorf("expected value \"value\", got %q", value)
		}
		setenvCalled = true
		return nil
	}
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "", false
	}
	unsetenv = func(name string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		unsetenvCalled = true
		return nil
	}
	t.Cleanup(func() {
		setenv = saveSetenv
		lookupenv = saveLookupenv
		unsetenv = saveUnsetenv
	})

	setEnv("ENV", nil)

	if setenvCalled {
		t.Error("Setenv called")
	}
	if !lookupenvCalled {
		t.Error("LookupEnv not called")
	}
	if unsetenvCalled {
		t.Error("Unsetenv called")
	}
}

func TestInternalSetEnvUnsetFails(t *testing.T) {
	saveSetenv := setenv
	saveLookupenv := lookupenv
	saveUnsetenv := unsetenv
	setenvCalled := false
	lookupenvCalled := false
	unsetenvCalled := false
	setenv = func(name, value string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if value != "value" {
			t.Errorf("expected value \"value\", got %q", value)
		}
		setenvCalled = true
		return nil
	}
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "", true
	}
	unsetenv = func(name string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		unsetenvCalled = true
		return assert.AnError
	}
	t.Cleanup(func() {
		setenv = saveSetenv
		lookupenv = saveLookupenv
		unsetenv = saveUnsetenv
	})

	panicData := tryRecover(func() { setEnv("ENV", nil) })

	if panicData == nil {
		t.Error("setEnv failed to panic")
	}
	if setenvCalled {
		t.Error("Setenv called")
	}
	if !lookupenvCalled {
		t.Error("LookupEnv not called")
	}
	if !unsetenvCalled {
		t.Error("Unsetenv not called")
	}
}

func TestInternalSetEnvSetenvSuccess(t *testing.T) {
	saveSetenv := setenv
	saveLookupenv := lookupenv
	saveUnsetenv := unsetenv
	setenvCalled := false
	lookupenvCalled := false
	unsetenvCalled := false
	setenv = func(name, value string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if value != "value" {
			t.Errorf("expected value \"value\", got %q", value)
		}
		setenvCalled = true
		return nil
	}
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "", true
	}
	unsetenv = func(name string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		unsetenvCalled = true
		return nil
	}
	t.Cleanup(func() {
		setenv = saveSetenv
		lookupenv = saveLookupenv
		unsetenv = saveUnsetenv
	})
	value := "value"

	setEnv("ENV", &value)

	if !setenvCalled {
		t.Error("Setenv not called")
	}
	if lookupenvCalled {
		t.Error("LookupEnv called")
	}
	if unsetenvCalled {
		t.Error("Unsetenv called")
	}
}

func TestInternalSetEnvSetenvFails(t *testing.T) {
	saveSetenv := setenv
	saveLookupenv := lookupenv
	saveUnsetenv := unsetenv
	setenvCalled := false
	lookupenvCalled := false
	unsetenvCalled := false
	setenv = func(name, value string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if value != "value" {
			t.Errorf("expected value \"value\", got %q", value)
		}
		setenvCalled = true
		return assert.AnError
	}
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "", true
	}
	unsetenv = func(name string) error {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		unsetenvCalled = true
		return nil
	}
	t.Cleanup(func() {
		setenv = saveSetenv
		lookupenv = saveLookupenv
		unsetenv = saveUnsetenv
	})
	value := "value"

	panicData := tryRecover(func() { setEnv("ENV", &value) })

	if panicData == nil {
		t.Error("setEnv failed to panic")
	}
	if !setenvCalled {
		t.Error("Setenv not called")
	}
	if lookupenvCalled {
		t.Error("LookupEnv called")
	}
	if unsetenvCalled {
		t.Error("Unsetenv called")
	}
}

func TestSetEnv(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})

	result := SetEnv(theT, "ENV", "value")

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if result.Name != "ENV" {
		t.Errorf("expected name \"ENV\", got %q", result.Name)
	}
	if *result.Value != "value" {
		t.Errorf("expected value \"value\", got %q", *result.Value)
	}
	if !applyCalled {
		t.Error("Apply not called")
	}
}

func TestUnsetEnv(t *testing.T) {
	theT := &mockT{}
	saveApply := apply
	applyCalled := false
	apply = func(tt T, _ Patcher) {
		if tt != theT {
			t.Error("apply not called with T value")
		}
		applyCalled = true
	}
	t.Cleanup(func() {
		apply = saveApply
	})

	result := UnsetEnv(theT, "ENV")

	if !theT.HelperCalled {
		t.Error("failed to call t.Helper")
	}
	if result.Name != "ENV" {
		t.Errorf("expected name \"ENV\", got %q", result.Name)
	}
	if result.Value != nil {
		t.Errorf("expected value nil, got %+v", result.Value)
	}
	if !applyCalled {
		t.Error("Apply not called")
	}
}

func TestEnvPatcherApplyBase(t *testing.T) {
	saveLookupenv := lookupenv
	saveSetEnvPatch := setEnvPatch
	lookupenvCalled := false
	setEnvPatchCalled := false
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "original", true // nolint:goconst
	}
	setEnvPatch = func(name string, value *string) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if *value != "value" {
			t.Errorf("expected value \"value\", got %q", *value)
		}
		setEnvPatchCalled = true
	}
	t.Cleanup(func() {
		lookupenv = saveLookupenv
		setEnvPatch = saveSetEnvPatch
	})
	value := "value"
	obj := &EnvPatcher{
		Name:  "ENV",
		Value: &value,
	}

	obj.Apply()

	if *obj.Original != "original" {
		t.Errorf("expected original \"original\", got %q", *obj.Original)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
	if !lookupenvCalled {
		t.Error("lookupenv not called")
	}
	if !setEnvPatchCalled {
		t.Error("setEnvPatch not called")
	}
}

func TestEnvPatcherApplyUnset(t *testing.T) {
	saveLookupenv := lookupenv
	saveSetEnvPatch := setEnvPatch
	lookupenvCalled := false
	setEnvPatchCalled := false
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "original", false
	}
	setEnvPatch = func(name string, value *string) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if *value != "value" {
			t.Errorf("expected value \"value\", got %q", *value)
		}
		setEnvPatchCalled = true
	}
	t.Cleanup(func() {
		lookupenv = saveLookupenv
		setEnvPatch = saveSetEnvPatch
	})
	value := "value"
	obj := &EnvPatcher{
		Name:  "ENV",
		Value: &value,
	}

	obj.Apply()

	if obj.Original != nil {
		t.Errorf("expected original nil, got %+v", obj.Original)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
	if !lookupenvCalled {
		t.Error("lookupenv not called")
	}
	if !setEnvPatchCalled {
		t.Error("setEnvPatch not called")
	}
}

func TestEnvPatcherApplyIdempotent(t *testing.T) {
	saveLookupenv := lookupenv
	saveSetEnvPatch := setEnvPatch
	lookupenvCalled := false
	setEnvPatchCalled := false
	lookupenv = func(name string) (string, bool) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		lookupenvCalled = true
		return "original", true
	}
	setEnvPatch = func(name string, value *string) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if *value != "value" {
			t.Errorf("expected value \"value\", got %q", *value)
		}
		setEnvPatchCalled = true
	}
	t.Cleanup(func() {
		lookupenv = saveLookupenv
		setEnvPatch = saveSetEnvPatch
	})
	value := "value"
	obj := &EnvPatcher{
		Name:    "ENV",
		Value:   &value,
		Applied: true,
	}

	obj.Apply()

	if obj.Original != nil {
		t.Errorf("expected original nil, got %+v", obj.Original)
	}
	if !obj.Applied {
		t.Error("Applied not set")
	}
	if lookupenvCalled {
		t.Error("lookupenv called")
	}
	if setEnvPatchCalled {
		t.Error("setEnvPatch called")
	}
}

func TestEnvPatcherRestoreBase(t *testing.T) {
	saveSetEnvPatch := setEnvPatch
	setEnvPatchCalled := false
	setEnvPatch = func(name string, value *string) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if *value != "original" {
			t.Errorf("expected value \"original\", got %q", *value)
		}
		setEnvPatchCalled = true
	}
	t.Cleanup(func() {
		setEnvPatch = saveSetEnvPatch
	})
	original := "original"
	obj := &EnvPatcher{
		Name:     "ENV",
		Original: &original,
		Applied:  true,
	}

	obj.Restore()

	if obj.Applied {
		t.Error("Applied set")
	}
	if !setEnvPatchCalled {
		t.Error("setEnvPatch not called")
	}
}

func TestEnvPatcherRestoreIdempotent(t *testing.T) {
	saveSetEnvPatch := setEnvPatch
	setEnvPatchCalled := false
	setEnvPatch = func(name string, value *string) {
		if name != "ENV" {
			t.Errorf("expected name \"ENV\", got %q", name)
		}
		if *value != "original" {
			t.Errorf("expected value \"original\", got %q", *value)
		}
		setEnvPatchCalled = true
	}
	t.Cleanup(func() {
		setEnvPatch = saveSetEnvPatch
	})
	original := "original"
	obj := &EnvPatcher{
		Name:     "ENV",
		Original: &original,
	}

	obj.Restore()

	if obj.Applied {
		t.Error("Applied set")
	}
	if setEnvPatchCalled {
		t.Error("setEnvPatch called")
	}
}
