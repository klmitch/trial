// Copyright (c) 2021 Kevin L. Mitchell
//
// Licensed under the Apache License, Version 2.0 (the "License"); you
// may not use this file except in compliance with the License.  You
// may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or
// implied.  See the License for the specific language governing
// permissions and limitations under the License.

package patch

import (
	"bytes"
	"fmt"
	"reflect"
)

// StreamPatcher is a patcher that, given a pointer to a stream
// variable (something that can accept a bytes.Buffer pointer), will
// point that variable to itself.  A StreamPatcher embeds a
// bytes.Buffer, so any bytes.Buffer method can be used with it.
type StreamPatcher struct {
	bytes.Buffer
	Variable reflect.Value // The variable to patch
	Original reflect.Value // The variable's original value
	Applied  bool          // Indicates the patch is applied
}

// bufferType is the reflect.TypeOf of a pointer to a StreamPatcher.
// It is used to validate that the variable can accept the patcher.
var bufferType = reflect.TypeOf(&StreamPatcher{})

// SetStream constructs and applies a StreamPatcher, setting the
// specified variable to point to the patcher.  It could be used in a
// test function like so:
//
//	var stderr io.Writer = os.Stderr
//
//	func TestDoSomething(t *testing.T) {
//		out := patch.SetStream(t, &stderr)
//
//		DoSomething("some argument")
//
//		assert.Equal(t, "some argument\n", out.String())
//	}
func SetStream(t T, variable interface{}) *StreamPatcher {
	// We're a helper function
	t.Helper()

	// Select the variable and validate it's a settable object
	varReflect := reflect.ValueOf(variable)
	if varReflect.Type().Kind() != reflect.Ptr {
		panic("cannot set variable passed to SetStream")
	}
	v := varReflect.Elem()
	if !bufferType.AssignableTo(v.Type()) {
		panic(fmt.Sprintf("cannot assign %s type to variable type %s", bufferType, v.Type()))
	}

	// Construct the patcher
	p := &StreamPatcher{
		Variable: v,
	}

	// Apply it
	apply(t, p)

	return p
}

// Apply applies the patch.  It is assumed that the Restore method
// will have sufficient data to restore the patch.
func (sp *StreamPatcher) Apply() {
	// Be idempotent
	if sp.Applied {
		return
	}

	// Save the current value of the variable
	sp.Original = reflect.ValueOf(sp.Variable.Interface())

	// Set the variable to the desired value
	sp.Variable.Set(reflect.ValueOf(sp))
	sp.Applied = true
}

// Restore restores the patch to its original setting.  It must be
// idempotent: if called a second time, there must be no effect.
func (sp *StreamPatcher) Restore() {
	// Be idempotent
	if !sp.Applied {
		return
	}

	// Restore the variable to the original value
	sp.Variable.Set(sp.Original)
	sp.Applied = false
}
